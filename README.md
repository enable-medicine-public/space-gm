# SPACE-GM

This is an example code base for SPACE-GM: geometric deep learning of disease-associated microenvironments from multiplex spatial protein profiles. DOI: [10.1101/2022.05.12.491707](https://doi.org/10.1101/2022.05.12.491707)

Our work is formally published at Nature Biomedical Engineering: Graph deep learning for the characterization of tumour microenvironments from spatial protein profiles in tissue specimens. DOI: [10.1038/s41551-022-00951-w](https://doi.org/10.1038/s41551-022-00951-w)

![SPACEGM_Image](https://gitlab.com/enable-medicine-public/space-gm/uploads/5d3c0452b8cd3742ea0acd9ffa05bb67/Artboard_1spacegm.png)

See `Example.ipynb` for a tutorial of using Graph Neural Networks (GNNs) to perform predictions on spatial cellular graphs.

See `Microenvironment_Clustering.ipynb` for an example analysis pipeline that uses a trained GNN to define microenvironment clusters.

## Installation
- from source:
    ```
    $ git clone https://gitlab.com/enable-medicine-public/space-gm.git
    $ cd space-gm
    $ python setup.py install
    ```

## Data Availability

Datasets used in the SPACE-GM manuscript are now available at:
- https://doi.org/10.5281/zenodo.13179600
- https://app.enablemedicine.com/portal/atlas-library/studies/92394a9f-6b48-4897-87de-999614952d94

## Requirements

- numpy
- scipy
- pandas
- networkx
- matplotlib
- scikit-learn
- geovoronoi
- pytorch >= 1.13.0
- pytorch_geometric >= 2.2.0
- CUDA 11.7 (if with GPU)
