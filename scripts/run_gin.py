import os
import torch
import spacegm
from spacegm.utils import CELL_TYPE_FREQ_UPMC, CELL_TYPE_MAPPING_UPMC, BIOMARKERS_UPMC


# Settings
raw_data_root = "upmc_data/raw_data"
dataset_root = "upmc_data/dataset"
use_node_features = ['cell_type', 'SIZE']
subgraph_size = 3  # n-hop subgraphs
graph_label_file = "upmc_data/upmc_labels.csv"
graph_tasks = ['primary_outcome']  # Subset of ['primary_outcome', 'recurred', 'hpvstatus']
device = 'cuda:0'

# Split dataset based on coverlip, two validation folds
fold0_coverslips = {'c003', 'c005'}
fold1_coverslips = {'c001', 'c006'}

# Generate cellular graphs from raw inputs
nx_graph_root = os.path.join(dataset_root, "graph")
fig_save_root = os.path.join(dataset_root, "fig")
model_save_root = os.path.join(dataset_root, "model")

region_ids = set([f.split('.')[0] for f in os.listdir(raw_data_root)])
os.makedirs(nx_graph_root, exist_ok=True)
os.makedirs(fig_save_root, exist_ok=True)
os.makedirs(model_save_root, exist_ok=True)

for region_id in region_ids:
    graph_output = os.path.join(nx_graph_root, "%s.gpkl" % region_id)
    if not os.path.exists(graph_output):
        print("Processing %s" % region_id)
        G = spacegm.construct_graph_for_region(
            region_id,
            cell_coords_file=os.path.join(raw_data_root, "%s.cell_data.csv" % region_id),
            cell_types_file=os.path.join(raw_data_root, "%s.cell_types.csv" % region_id),
            cell_biomarker_expression_file=os.path.join(raw_data_root, "%s.expression.csv" % region_id),
            cell_features_file=os.path.join(raw_data_root, "%s.cell_features.csv" % region_id),
            voronoi_file=os.path.join(raw_data_root, "%s.json" % region_id),
            graph_output=graph_output,
            voronoi_polygon_img_output=None,
            graph_img_output=os.path.join(fig_save_root, "%s_graph.png" % region_id),
            figsize=10)

# Define Cellular Graph Dataset
dataset_kwargs = {
    'raw_folder_name': 'graph',
    'processed_folder_name': 'tg_graph',
    'node_features': ["cell_type", "SIZE", "biomarker_expression", "neighborhood_composition", "center_coord"],
    'edge_features': ["edge_type", "distance"],
    'cell_type_mapping': CELL_TYPE_MAPPING_UPMC,
    'cell_type_freq': CELL_TYPE_FREQ_UPMC,
    'biomarkers': BIOMARKERS_UPMC,
    'subgraph_size': subgraph_size,
    'subgraph_source': 'chunk_save',
    'subgraph_allow_distant_edge': True,
    'subgraph_radius_limit': 55 * subgraph_size + 35,
}
feature_kwargs = {
    "biomarker_expression_process_method": "linear",
    "biomarker_expression_lower_bound": -2,
    "biomarker_expression_upper_bound": 3,
    "neighborhood_size": 10,
}
dataset_kwargs.update(feature_kwargs)
dataset = spacegm.CellularGraphDataset(dataset_root, **dataset_kwargs)
dataset.save_all_subgraphs_to_chunk()

# Define Transformers
transformers = [
    spacegm.FeatureMask(dataset,
                        use_center_node_features=use_node_features,
                        use_neighbor_node_features=use_node_features),
    spacegm.AddGraphLabel(graph_label_file, tasks=graph_tasks)
]
dataset.set_transforms(transformers)

# Define train/valid split
region_ids = [dataset.get_full(i).region_id for i in range(dataset.N)]
coverslip_ids = [r_id.split('_')[1] for r_id in region_ids]
split_indices = [0 if cs_id in fold0_coverslips else
                 1 if cs_id in fold1_coverslips else
                 2 for cs_id in coverslip_ids]

for fold in [0, 1]:
    # Define Model kwargs
    model_kwargs = {
        'num_layer': dataset.subgraph_size,
        'num_node_type': len(dataset.cell_type_mapping) + 1,
        'num_feat': dataset[0].x.shape[1] - 1,
        'emb_dim': 512,
        'num_node_tasks': 0,
        'num_graph_tasks': len(graph_tasks),
        'node_embedding_output': 'last',
        'drop_ratio': 0.25,
        'graph_pooling': "max",
        'gnn_type': 'gin',
    }
    # Define Train and Evaluate kwargs
    train_kwargs = {
        'batch_size': 64,
        'lr': 0.001,
        'graph_loss_weight': 1.0,
        'num_iterations': 5e4,
        'num_graphs_per_segment': 32,
        'num_iterations_per_segment': 5e3,

        'node_task_loss_fn': None,
        'graph_task_loss_fn': spacegm.models.BinaryCrossEntropy(),
        'evaluate_fn': [spacegm.train.evaluate_by_full_graph, spacegm.train.save_model_weight],
        'evaluate_freq': 5e3
    }
    evaluate_kwargs = {
        'shuffle': True,
        'subsample_ratio': 0.1,
        'full_graph_graph_task_evaluate_fn': spacegm.inference.full_graph_graph_classification_evaluate_fn,
        'score_file': os.path.join(model_save_root, 'GIN-%s-%d.txt' % (graph_tasks[0], fold)),
        'model_folder': os.path.join(model_save_root, 'GIN-%s-%d' % (graph_tasks[0], fold)),
    }
    train_kwargs.update(evaluate_kwargs)
    os.makedirs(evaluate_kwargs['model_folder'], exist_ok=True)

    train_inds = [i for i, s in enumerate(split_indices) if s != fold]
    valid_inds = [i for i, s in enumerate(split_indices) if s == fold]

    # Model initialization and training
    model = spacegm.models.GNN_pred(**model_kwargs)
    model = spacegm.train.train_subgraph(
        model, dataset, device,
        train_inds=train_inds, valid_inds=valid_inds, **train_kwargs)
    spacegm.train.evaluate_by_full_graph(
        model, dataset, device,
        train_inds=train_inds, valid_inds=valid_inds, **evaluate_kwargs)
    torch.save(model.state_dict(),
               os.path.join(evaluate_kwargs['model_folder'], 'model_save_final.pt'))
